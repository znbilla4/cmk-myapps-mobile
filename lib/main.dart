import 'package:flutter/material.dart';
import 'package:myappsnew/Screens/Login/login.dart';
import 'package:myappsnew/Screens/Main_Screen/main_screen.dart';
import 'package:myappsnew/Screens/Profile/profile.dart';

void main() {
  runApp(const AppStart());
}

class AppStart extends StatelessWidget {
  const AppStart({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primaryColor:
            Color.fromARGB(255, 255, 215, 0), // Set the primary color here
        highlightColor: Colors.yellow, // Set the highlight color to yellow
      ),
      home: LoginScreen(),
    );
  }
}

class PageNavigator {
  static void navigateToPage(BuildContext context, int index) {
    late Widget page;

    switch (index) {
      case 0:
        page = MainScreen(
          onLoginSuccess: () {},
        );
        break;
      case 1:
        page = const ProfilePage();
        break;
      // Add cases for other pages
    }

    Navigator.of(context).push(MaterialPageRoute(builder: (context) => page));
  }
}
