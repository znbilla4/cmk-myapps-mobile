import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_custom_carousel_slider/flutter_custom_carousel_slider.dart';
import 'package:google_nav_bar/google_nav_bar.dart';
import 'package:myappsnew/Data/User.dart';
import 'package:myappsnew/Screens/Drawser/drawer.dart';
import 'package:myappsnew/Screens/Profile/profile.dart';
import 'package:myappsnew/Screens/assets/notif.dart';
import 'package:myappsnew/Screens/componets/calender/cal.dart';
import 'package:myappsnew/Screens/componets/main_screen_widgets/TopWidget.dart';
import 'package:myappsnew/Screens/componets/main_screen_widgets/appbar.dart';
import 'package:myappsnew/Screens/componets/main_screen_widgets/carousell.dart';
import 'package:myappsnew/Screens/componets/main_screen_widgets/daily_greet.dart';
import 'package:myappsnew/Screens/componets/main_screen_widgets/date.dart';
import 'package:myappsnew/Screens/componets/main_screen_widgets/event.dart';
import 'package:myappsnew/Screens/componets/main_screen_widgets/greeting.dart';
import 'package:myappsnew/Screens/componets/main_screen_widgets/nav_icons.dart';
import 'package:myappsnew/Screens/componets/main_screen_widgets/news.dart';
import 'package:myappsnew/Screens/componets/main_screen_widgets/widget_new.dart';

import '../../res/cons.dart';

class MainScreen extends StatefulWidget {
  final VoidCallback onLoginSuccess;

  MainScreen({required this.onLoginSuccess, Key? key}) : super(key: key);

  @override
  State<MainScreen> createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  bool _showAlert = false;
  String userName = 'John'; // Set the user's name
  String userBirthday = 'August 8'; // Set the user's birthday
  List<UserData> userDataList = []; // List of user data

  int _selectedIndex = 0;
  static const TextStyle optionStyle =
      TextStyle(fontSize: 30, fontWeight: FontWeight.w600);

  late PageController _pageController;
  late double _scale;

  void _handleLoginSuccess() {
    setState(() {
      // ... (your other state changes)
      _showAlert = true;
    });

    // Show the birthday wishes alert if _showAlert is true
    if (_showAlert) {
      Future.delayed(Duration.zero, () {
        _showMyDialog(); // Call your custom dialog method
      });
    }
  }

  void _showMyDialog() {
    showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('Profiles'),
          content: Container(
            height: 320,
            width: double.minPositive,
            child: GridView.builder(
              shrinkWrap: true,
              itemCount: userDataList.length,
              gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2, // Number of columns
                crossAxisSpacing: 8.0,
                mainAxisSpacing: 8.0,
              ),
              itemBuilder: (BuildContext context, int index) {
                UserData user = userDataList[index];
                return Column(
                  children: [
                    ListTile(
                      //  leading: Icon(Icons.person),
                      title: const Icon(Icons.person),
                      subtitle: Text('${user.birthday}'),
                    ),
                    const Text('Greet')
                  ],
                );
              },
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: const Text('Close'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  @override
  void initState() {
    super.initState();

    // Populate the user data list with sample data
    userDataList = [
      UserData(name: 'John', birthday: 'August 8'),
      UserData(name: 'Jane', birthday: 'August 9'),
      UserData(name: 'John', birthday: 'August 8'),
      UserData(name: 'Jane', birthday: 'August 9'),
      UserData(name: 'John', birthday: 'August 8'),
      UserData(name: 'Jane', birthday: 'August 9'),
      UserData(name: 'John', birthday: 'August 8'),
      UserData(name: 'Jane', birthday: 'August 9'),
      UserData(name: 'John', birthday: 'August 8'),
      UserData(name: 'Jane', birthday: 'August 9'),
      // Add more user data entries here
    ];

    // Call _handleLoginSuccess when the page loads
    _handleLoginSuccess();

    super.initState();
    _selectedIndex = 0;
    _pageController = PageController(initialPage: _selectedIndex);
    _scale = 1.0;
  }

  @override
  Widget build(BuildContext context) {
    // It provide us total height and width
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: backgroundColor,
      // appBar: AppBar(
      //   toolbarHeight: 80,
      //   flexibleSpace: Image(
      //     image: AssetImage('assets/images/ACHIEBG.png'),
      //     fit: BoxFit.cover,
      //   ),
      //   backgroundColor: Colors.transparent,
      //   elevation: 20, // Remove shadow
      //   titleSpacing: 20, // Adjust title spacing if needed
      //   //  centerTitle: true,
      //   title: Text('Hi M.Fauzi'),
      //   actions: [
      //     IconButton(
      //       icon: const Icon(Icons.notifications_active, size: 25),
      //       onPressed: () {
      //         NotificationDialog.show(context);
      //       },
      //     ),
      //     IconButton(
      //       icon: const CircleAvatar(
      //         backgroundImage: AssetImage(
      //             'assets/images/profilepic.webp'), // Replace with your profile picture image path
      //       ),
      //       onPressed: () {
      //         Navigator.push(
      //           context,
      //           MaterialPageRoute(builder: (context) => const ProfilePage()),
      //         );
      //       },
      //     ),
      //   ],
      // ),
      bottomNavigationBar: Container(
        decoration: const BoxDecoration(
          color: Colors.white,
          boxShadow: [],
        ),
        child: GNav(
            rippleColor:
                Colors.grey[800]!, // tab button ripple color when pressed
            hoverColor: Colors.grey[700]!, // tab button hover color
            haptic: true, // haptic feedback
            tabBorderRadius: 15,
            curve: Curves.easeOutExpo, // tab animation curves
            //duration: Duration(milliseconds: 90), // tab animation duration
            gap: 8, // the tab button gap between icon and text
            color: Colors.grey[800], // unselected icon color
            activeColor: const Color.fromARGB(
                255, 0, 0, 0), // selected icon and text color
            iconSize: 30, // tab button icon size
            tabBackgroundColor: const Color.fromARGB(255, 189, 183, 0)
                .withOpacity(0.1), // selected tab background color
            padding: const EdgeInsets.symmetric(
                horizontal: 20, vertical: 15), // navigation bar padding
            tabs: [
              const GButton(
                icon: Icons.dashboard,
                text: 'Dashboard',
              ),
              const GButton(
                icon: Icons.event,
                text: 'Event',
              ),
              const GButton(
                icon: Icons.newspaper,
                text: 'News',
              ),
              const GButton(
                icon: Icons.person,
                text: 'Profile',
              ),
            ],
            selectedIndex: _selectedIndex,
            onTabChange: (index) {
              setState(() {
                _selectedIndex = index;
              });
            }),
      ),
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        physics: AlwaysScrollableScrollPhysics(),
        child: Column(
          children: [
            SizedBox(
              height: size.height,
              child: Stack(
                children: [
                  Container(
                    // margin: EdgeInsets.only(top: size.height * 0.3),
                    padding: EdgeInsets.only(
                      //top: size.height * 0.12,
                      left: 0,
                      right: 0,
                    ),
                    // height: 500,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(24),
                        topRight: Radius.circular(24),
                      ),
                    ),
                    child: Column(
                      children: [
                        AppBarAlt(),
                        NavIcons(),
                        // Greeting(),
                        // SizedBox(
                        //   height: 20,
                        // ),
                        Carousell(),
                        Greeting(),
                      ],
                    ),
                  )
                ],
              ),
            ),

            // CustomCarouselSlider(
            //   items: itemList,
            //   height: 300,
            //   subHeight: 100,
            //   width: MediaQuery.of(context).size.width * .9,
            //   autoplay: true,
            // ),
          ],
        ),
      ),
      //   drawer: const MyDrawer(),
      // bottomNavigationBar:
    );
  }
}
