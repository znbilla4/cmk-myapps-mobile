import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:myappsnew/Screens/Main_Screen/view_model/home%20bloc/home_states.dart';

class HomeBloc extends Cubit<HomeStates> {
  HomeBloc() : super(TabTwo());

  void changeTab(int index) {
    if (index == 0) {
      emit(TabOne());
    } else if (index == 1) {
      emit(TabTwo());
    } else if (index == 2) {
      emit(TabThree());
    } else {
      emit(TabFour());
    }
  }
}
