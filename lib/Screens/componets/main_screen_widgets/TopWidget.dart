import 'package:flutter/material.dart';

class TopWidget extends StatelessWidget {
  const TopWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(12),
      ),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(12),
        child: Container(
          decoration: const BoxDecoration(
            image: DecorationImage(
              image: AssetImage('assets/images/ACHIEBG.png'),
              fit: BoxFit.cover,
            ),
          ),
          child: Padding(
            padding: const EdgeInsets.all(12.0),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text("Our Monthly Performance",
                        style: TextStyle(color: Colors.yellow, fontSize: 20)),
                    Text("May 2023",
                        style: TextStyle(color: Colors.white, fontSize: 15))
                  ],
                ),
                Card(
                  color: Color.fromARGB(53, 255, 255, 255),
                  child: ListTile(
                      leading: Container(
                          height: 35,
                          width: 90,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(60))),
                          child: Padding(
                            padding: const EdgeInsets.all(5.0),
                            child: Image.asset('assets/images/TP.png'),
                          )),
                      title: Text('5,9 M',
                          style: TextStyle(color: Colors.yellow, fontSize: 25)),
                      subtitle: Text("Best Performance: 7,2 M on March 2023",
                          style: TextStyle(color: Colors.white))),
                ),
                Card(
                  color: Color.fromARGB(53, 255, 255, 255),
                  child: ListTile(
                    leading: Container(
                        height: 45,
                        width: 95,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius:
                                BorderRadius.all(Radius.circular(60))),
                        child: Padding(
                          padding: const EdgeInsets.all(5.0),
                          child: Image.asset('assets/images/F.png'),
                        )),
                    title: Text('5,9 M',
                        style: TextStyle(color: Colors.yellow, fontSize: 25)),
                    subtitle: Text("Best Performance: 7,2 M on March 2023",
                        style: TextStyle(color: Colors.white)),
                  ),
                ),
                Card(
                  color: Color.fromARGB(53, 255, 255, 255),
                  child: ListTile(
                    leading: Container(
                        height: 35,
                        width: 95,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius:
                                BorderRadius.all(Radius.circular(60))),
                        child: Padding(
                          padding: const EdgeInsets.all(5.0),
                          child: Image.asset('assets/images/MD.png'),
                        )),
                    title: Text(
                      '5,9 M',
                      style: TextStyle(color: Colors.yellow, fontSize: 25),
                    ),
                    subtitle: Text(
                      "Best Performance: 7,2 M on March 2023",
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
