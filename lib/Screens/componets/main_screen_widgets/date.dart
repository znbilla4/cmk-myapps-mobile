import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class DateScreen extends StatefulWidget {
  const DateScreen({Key? key}) : super(key: key);

  @override
  _DateScreenState createState() => _DateScreenState();
}

class _DateScreenState extends State<DateScreen> {
  final DateFormat _dateFormat = DateFormat('E');
  final DateFormat _dateFormatdate = DateFormat('dd');
  double itemHeight = 75;
  @override
  Widget build(BuildContext context) {
    List<Map<String, String>> dummyList = [
      {
        'name': 'Meeting with Vendor',
        'email': '10:30',
      },
      {
        'name': 'Meeting with IT',
        'email': '10:30',
      },
      {
        'name': 'Meeting Weekly',
        'email': '10:30',
      },
    ];
    DateTime currentDate = DateTime.now();
    DateFormat monthFormat = DateFormat('MMMM');
    String formattedMonth = monthFormat.format(currentDate);
    DateFormat yearFormat = DateFormat('yyyy');
    String formattedYear = yearFormat.format(currentDate);

    DateTime datePlus1 = currentDate.add(const Duration(days: 1));
    DateTime datePlus2 = currentDate.add(const Duration(days: 2));
    DateTime datePlus3 = currentDate.add(const Duration(days: 3));

    DateTime dateMinus1 = currentDate.subtract(const Duration(days: 1));
    DateTime dateMinus2 = currentDate.subtract(const Duration(days: 2));
    DateTime dateMinus3 = currentDate.subtract(const Duration(days: 3));

    DateTime dateDPlus1 = currentDate.add(const Duration(days: 1));
    DateTime dateDPlus2 = currentDate.add(const Duration(days: 2));
    DateTime dateDPlus3 = currentDate.add(const Duration(days: 3));

    DateTime dateDMinus1 = currentDate.subtract(const Duration(days: 1));
    DateTime dateDMinus2 = currentDate.subtract(const Duration(days: 2));
    DateTime dateDMinus3 = currentDate.subtract(const Duration(days: 3));
    return Card(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  formattedMonth,
                  style: const TextStyle(
                      fontWeight: FontWeight.bold, fontSize: 24),
                ),
                Text(formattedYear)
              ],
            ),
            const Divider(
              color: Colors.grey, // Specify the color of the line
              thickness: 0.5, // Specify the thickness or height of the line
            ),
            Row(),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Column(
                  children: [
                    Text(_dateFormat.format(dateMinus3)),
                    const SizedBox(
                      height: 10,
                    ),
                    Text(_dateFormatdate.format(dateDMinus3))
                  ],
                ),
                Column(
                  children: [
                    Text(_dateFormat.format(dateMinus2)),
                    const SizedBox(
                      height: 10,
                    ),
                    Text(_dateFormatdate.format(dateDMinus2))
                  ],
                ),
                Column(
                  children: [
                    Text(_dateFormat.format(dateMinus1)),
                    const SizedBox(
                      height: 10,
                    ),
                    Text(_dateFormatdate.format(dateDMinus1))
                  ],
                ),
                Card(
                    child: Column(
                  children: [
                    Text(_dateFormat.format(currentDate)),
                    Container(
                      decoration: const BoxDecoration(
                        shape: BoxShape.circle,
                        color: Color.fromARGB(255, 236, 201, 1),
                      ),
                      padding: const EdgeInsets.all(10),
                      child: Text(
                        _dateFormatdate.format(currentDate),
                        style: const TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ],
                )),
                Column(
                  children: [
                    Text(_dateFormat.format(dateDPlus1)),
                    const SizedBox(
                      height: 10,
                    ),
                    Text(_dateFormatdate.format(datePlus1))
                  ],
                ),
                Column(
                  children: [
                    Text(_dateFormat.format(dateDPlus2)),
                    const SizedBox(
                      height: 10,
                    ),
                    Text(_dateFormatdate.format(datePlus2))
                  ],
                ),
                Column(
                  children: [
                    Text(_dateFormat.format(dateDPlus3)),
                    const SizedBox(
                      height: 10,
                    ),
                    Text(_dateFormatdate.format(datePlus3))
                  ],
                ),
              ],
            ),
            SizedBox(
              height: dummyList.length * itemHeight,
              child: ListView.builder(
                itemCount: dummyList.length,
                shrinkWrap: true,
                itemBuilder: (context, index) {
                  return Card(
                    child: Container(
                      decoration: BoxDecoration(
                        border: Border(
                          left: BorderSide(
                              width: 8.0, color: Colors.lightBlue.shade600),
                        ),
                        color: Colors.white,
                      ),
                      child: ListTile(
                        // leading: Icon(Icons.person),
                        title: Text(dummyList[index]['name'] ?? ''),
                        subtitle: Row(
                          children: [
                            const Icon(Icons.watch_later_rounded),
                            Text(' ${dummyList[index]['email'] ?? ''}')
                          ],
                        ),
                        // subtitle: Text(dummyList[index]['email'] ?? ''),
                        // trailing: Icon(Icons.arrow_forward),
                      ),
                    ),
                  );
                },
              ),
            ),
            TextButton(
                onPressed: () {},
                child: const Text(
                  'Open My Calendar',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Color.fromARGB(255, 228, 205, 0),
                  ),
                ))
          ],
        ),
      ),
    );
  }
}
