import 'package:flutter/material.dart';
import 'package:myappsnew/Screens/Profile/profile.dart';
import 'package:myappsnew/Screens/assets/notif.dart';

class AppBarAlt extends StatelessWidget {
  const AppBarAlt({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 100,
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
          borderRadius: const BorderRadius.only(
              bottomRight: Radius.circular(20),
              bottomLeft: Radius.circular(20)),
          boxShadow: [
            BoxShadow(
              color: Color.fromARGB(255, 43, 43, 43)!,
              offset: const Offset(0, 15),
              blurRadius: 30,
              spreadRadius: -5,
            ),
          ],
          gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [
                Color.fromARGB(255, 44, 44, 44)!,
                Color.fromARGB(255, 55, 55, 55)!,
                Color.fromARGB(255, 65, 65, 65)!,
                Color.fromARGB(255, 85, 85, 85)!,
              ],
              stops: const [
                0.1,
                0.3,
                0.5,
                0.8
              ])),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              children: [Text("Hi."), Text("User name here")],
            ),
            Row(
              children: [
                IconButton(
                  icon: Icon(Icons.notifications_active, size: 25),
                  onPressed: () {
                    NotificationDialog.show(context);
                  },
                ),
                IconButton(
                  icon: const CircleAvatar(
                    backgroundImage: AssetImage(
                        'assets/images/profilepic.webp'), // Replace with your profile picture image path
                  ),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => const ProfilePage()),
                    );
                  },
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
