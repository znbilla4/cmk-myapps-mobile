import 'package:flutter/material.dart';

class NavIcons extends StatelessWidget {
  final List<IconData> icons = [
    Icons.home,
    Icons.search,
    Icons.favorite,
    Icons.settings,
    Icons.mail,
    Icons.person,
    Icons.photo,
    Icons.map,
  ];

  NavIcons({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          // border: Border.all(width: 1),
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(50), topRight: Radius.circular(50))),
      height: 180,
      width: double.infinity,
      child: GridView.builder(
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          childAspectRatio: 1.5,
          crossAxisSpacing: 10,
          mainAxisSpacing: 20,
          crossAxisCount: 4,
        ),
        itemCount: icons.length,
        itemBuilder: (BuildContext context, int index) {
          return Padding(
            padding: const EdgeInsets.only(left: 10.0, right: 10),
            child: Container(
              decoration: BoxDecoration(
                  color: Colors.black12,
                  //border: Border.all(width: 1),
                  borderRadius: BorderRadius.all(Radius.circular(10))),
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Icon(
                      icons[index],
                      color: Color.fromARGB(255, 254, 203, 0),
                      size: 27,
                      fill: 1,
                      //weight: 201,
                    ),
                  ),
                  Text('Nama')
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
