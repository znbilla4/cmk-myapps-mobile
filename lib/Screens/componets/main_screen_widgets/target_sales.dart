import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

class _SplineAreaData {
  _SplineAreaData(this.month, this.y1, this.y2, this.y3);
  final String month;
  final double y1;
  final double y2;
  final double y3;
}

class SplineArea extends StatefulWidget {
  const SplineArea({super.key});

  @override
  State<SplineArea> createState() => _SplineAreaStateState();
}

class _SplineAreaStateState extends State<SplineArea> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        const Text(
          "Target Sales",
          style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
        ),
        Card(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Column(
                      children: const [
                        Text(
                          'Frank & Co',
                          style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        SizedBox(height: 5),
                        Text(
                          '+10%',
                          style: TextStyle(
                            fontSize: 16,
                            color: Colors.green,
                          ),
                        ),
                      ],
                    ),
                    Column(
                      children: const [
                        Text(
                          'The Palace',
                          style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        SizedBox(height: 5),
                        Text(
                          '+10%',
                          style: TextStyle(
                            fontSize: 16,
                            color: Colors.green,
                          ),
                        ),
                      ],
                    ),
                    Column(
                      children: const [
                        Text(
                          'Mondial',
                          style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        SizedBox(height: 5),
                        Text(
                          '+10%',
                          style: TextStyle(
                            fontSize: 16,
                            color: Colors.green,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
                _buildSplineAreaChart(),
              ],
            ),
          ),
        )
      ],
    );
  }

  /// Returns the cartesian spline area chart.
  SfCartesianChart _buildSplineAreaChart() {
    return SfCartesianChart(
      legend: Legend(
        isVisible: true,
        opacity: 0.7,
        position: LegendPosition.top,
      ),
      //title: ChartTitle(text: 'This Month', alignment: ChartAlignment.near),
      plotAreaBorderWidth: 0,
      primaryXAxis: CategoryAxis(
        majorGridLines: const MajorGridLines(width: 0),
        labelRotation: -45,
      ),
      primaryYAxis: NumericAxis(
        labelFormat: '{value}%',
        axisLine: const AxisLine(width: 0),
        majorTickLines: const MajorTickLines(size: 0),
      ),
      series: _getSplineAreaSeries(),
      tooltipBehavior: TooltipBehavior(enable: true),
    );
  }

  List<_SplineAreaData>? chartData;

  @override
  void initState() {
    chartData = <_SplineAreaData>[
      _SplineAreaData('Jan', 10.53, 3.3, 0.3),
      _SplineAreaData('Feb', 9.5, 5.4, 2.3),
      _SplineAreaData('Mar', 10, 2.65, 4.3),
      _SplineAreaData('Apr', 9.4, 2.62, 8.3),
      _SplineAreaData('May', 5.8, 1.99, 1.3),
      _SplineAreaData('Jun', 4.9, 1.44, 2.3),
      _SplineAreaData('Jul', 4.5, 2, 3.3),
      _SplineAreaData('Aug', 3.6, 1.56, 1.3),
      _SplineAreaData('Sep', 3.43, 2.1, 8),
    ];
    super.initState();
  }

  /// Returns the list of chart series
  /// which need to render on the spline area chart.
  List<ChartSeries<_SplineAreaData, String>> _getSplineAreaSeries() {
    return <ChartSeries<_SplineAreaData, String>>[
      SplineAreaSeries<_SplineAreaData, String>(
        dataSource: chartData!,
        color: const Color.fromRGBO(75, 135, 185, 0.6),
        borderColor: const Color.fromRGBO(75, 135, 185, 1),
        borderWidth: 2,
        name: 'Frank and Co',
        xValueMapper: (_SplineAreaData data, _) => data.month,
        yValueMapper: (_SplineAreaData data, _) => data.y1,
      ),
      SplineAreaSeries<_SplineAreaData, String>(
        dataSource: chartData!,
        borderColor: const Color.fromRGBO(192, 108, 132, 1),
        color: const Color.fromRGBO(192, 108, 132, 0.6),
        borderWidth: 2,
        name: 'Palace',
        xValueMapper: (_SplineAreaData data, _) => data.month,
        yValueMapper: (_SplineAreaData data, _) => data.y2,
      ),
      SplineAreaSeries<_SplineAreaData, String>(
        dataSource: chartData!,
        borderColor: const Color.fromARGB(255, 0, 71, 204),
        color: const Color.fromARGB(153, 0, 183, 255),
        borderWidth: 2,
        name: 'Mondial',
        xValueMapper: (_SplineAreaData data, _) => data.month,
        yValueMapper: (_SplineAreaData data, _) => data.y3,
      ),
    ];
  }

  @override
  void dispose() {
    chartData!.clear();
    super.dispose();
  }
}
