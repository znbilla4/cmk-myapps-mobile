import 'package:flutter/material.dart';

class NewsWidget extends StatelessWidget {
  NewsWidget({Key? key}) : super(key: key);

  final List<Map<String, String>> newsItems = [
    {
      'title': 'Event 1',
      'description': 'Description of Event 1',
      'imageUrl': 'assets/images/Cat.jpg',
    },
    {
      'title': 'Event 2',
      'description': 'Description of Event 2',
      'imageUrl': 'assets/images/Cat.jpg',
    },
    {
      'title': 'Event 3',
      'description': 'Description of Event 3',
      'imageUrl': 'assets/images/Cat.jpg',
    }
    // Add more items as needed
  ];

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 1),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Row(
                    children: [
                      Icon(Icons.newspaper),
                      Text(
                        'News',
                        style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ],
                  ),
                  TextButton(
                      onPressed: () {},
                      child: const Text(
                        "View All",
                        style:
                            TextStyle(color: Color.fromARGB(255, 255, 215, 0)),
                      ))
                ],
              ),
            ),
            const Divider(
              thickness: 2,
            ),
            SizedBox(
              height: 7,
            ),
            SizedBox(
              height: 200,
              child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: newsItems.length,
                itemBuilder: (context, index) {
                  Map<String, String> newsItem = newsItems[index];
                  return Container(
                    width: 200,
                    margin: const EdgeInsets.symmetric(horizontal: 8),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Expanded(
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(8),
                              image: DecorationImage(
                                image:
                                    AssetImage(newsItems[index]['imageUrl']!),
                                fit: BoxFit.cover,
                              ),
                            ),
                          ),
                        ),
                        const SizedBox(height: 8),
                        Text(
                          newsItem['title']!,
                          style: const TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        const SizedBox(height: 4),
                        Text(
                          newsItem['description']!,
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                        ),
                      ],
                    ),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
