import 'package:flutter/material.dart';
import 'package:simple_gradient_text/simple_gradient_text.dart';


class TopAche extends StatelessWidget {
  const TopAche({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(12),
      ),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(12),
        child: Container(
          decoration: const BoxDecoration(
            image: DecorationImage(
              image: AssetImage('assets/images/ACHIEBG.png'),
              fit: BoxFit.cover,
            ),
          ),
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children:  [
                    GradientText(
    'Our Top\nAchievement',
    style: const TextStyle(
        fontSize: 30.0,
    ),
    colors: [
        Colors.yellowAccent,
        Color.fromARGB(255, 224, 170, 20),
        Color.fromARGB(255, 210, 231, 21),
    ],
),
                    const SizedBox(height: 8),
                    const Text(
                      "A wonderful serenity has\ntaken possession of my entire soul",
                      style: TextStyle(
                        color: Colors.white,
                      ),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 8,
                  width: 20,
                ),
                SizedBox(
                  width: 100,
                  height: 120,
                  child: Image.asset(
                    "assets/images/TopBG.png",
                    fit: BoxFit.fill,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
