import 'package:flutter/material.dart';

class User {
  final String name;
  final DateTime dateOfBirth;
  bool greeted;

  User({required this.name, required this.dateOfBirth, this.greeted = false});
}

class GreetingList extends StatefulWidget {
  const GreetingList({Key? key}) : super(key: key);

  @override
  _GreetingListState createState() => _GreetingListState();
}

class _GreetingListState extends State<GreetingList> {
  List<User> users = [
    User(name: 'John Doe', dateOfBirth: DateTime(2023, 6, 21)),
    User(name: 'Jane Smith', dateOfBirth: DateTime(2023, 5, 30)),
    User(name: 'Honest Rhinos', dateOfBirth: DateTime(1987, 9, 15)),
  ];

  List<User> getTodayBirthdays() {
    DateTime now = DateTime.now();
    int currentMonth = now.month;
    int currentDay = now.day;

    return users.where((user) {
      int userMonth = user.dateOfBirth.month;
      int userDay = user.dateOfBirth.day;
      return userMonth == currentMonth && userDay == currentDay;
    }).toList();
  }

  void _showConfirmationPopup(User user) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('Confirmation'),
          content: Text('Do you want to wish ${user.name} a happy birthday?'),
          actions: [
            TextButton(
              onPressed: () {
                Navigator.of(context).pop(); // Close the dialog
                _showThankYouPopup(user);
              },
              child: const Text('Send Greeting'),
            ),
            TextButton(
              onPressed: () {
                Navigator.of(context).pop(); // Close the dialog
              },
              child: const Text('Cancel'),
            ),
          ],
        );
      },
    );
  }

  void _showThankYouPopup(User user) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('Thank You!'),
          content: Text('Thank you for your greetings to ${user.name}!'),
          actions: [
            TextButton(
              onPressed: () {
                Navigator.of(context).pop(); // Close the dialog
              },
              child: const Text('OK'),
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    List<User> todayBirthdays = getTodayBirthdays();
    double hightofbox = 80;

    return todayBirthdays.isEmpty
        ? Container() // Empty container if no birthdays today
        : Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Text(
                    "Happy Birthday!",
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                  ),
                  TextButton(
                      onPressed: () {}, child: Text("Send Your Greetings"))
                ],
              ),
              Card(
                child: SizedBox(
                  height: todayBirthdays.length * hightofbox,
                  width: 400,
                  child: ListView.builder(
                    itemCount: todayBirthdays.length,
                    itemBuilder: (BuildContext context, int index) {
                      User user = todayBirthdays[index];
                      return ListTile(
                        leading: const Icon(Icons.person),
                        title: Text(user.name),
                        subtitle: Text(
                            user.greeted ? 'You greeted!' : 'Click to greet'),
                        trailing: FilledButton.tonal(
                            style: ButtonStyle(
                                backgroundColor:
                                    MaterialStateProperty.all(Colors.yellow)),
                            onPressed: () {},
                            child: const Text('Greet')),
                      );
                    },
                  ),
                ),
              ),
            ],
          );
  }
}
