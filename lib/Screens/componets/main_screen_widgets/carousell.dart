import 'package:flutter/material.dart';
import 'package:flutter_custom_carousel_slider/flutter_custom_carousel_slider.dart';
import 'package:myappsnew/Data/grid_news.dart';

class Carousell extends StatelessWidget {
  const Carousell({super.key});

  @override
  Widget build(BuildContext context) {
    List<CarouselItem> itemList = [
      CarouselItem(
        image: const ExactAssetImage('assets/images/ddbg.jpg'),
        boxDecoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(40)),
          // gradient: LinearGradient(
          //   begin: FractionalOffset.bottomCenter,
          //   end: FractionalOffset.topCenter,
          //   colors: [
          //     Colors.blueAccent.withOpacity(1),
          //     Colors.black.withOpacity(.3),
          //   ],
          //   stops: const [0.0, 1.0],
          // ),
        ),
        title: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
        titleTextStyle: const TextStyle(
          fontSize: 12,
          color: Colors.white,
        ),
        leftSubtitle: 'Jewellery',
        rightSubtitle: '08/14/2023',
        rightSubtitleTextStyle: const TextStyle(
          fontSize: 12,
          color: Colors.black,
        ),
        onImageTap: (i) {},
      ),
      CarouselItem(
        image: const ExactAssetImage('assets/images/Stock1.jpeg'),
        title: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
        titleTextStyle: const TextStyle(
          fontSize: 12,
          color: Colors.white,
        ),
        leftSubtitle: 'Jewellery',
        rightSubtitle: '08/14/2023',
        onImageTap: (i) {},
      ),
      CarouselItem(
        image: const ExactAssetImage('assets/images/newsbg1.png'),
        title: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
        titleTextStyle: const TextStyle(
          fontSize: 12,
          color: Colors.white,
        ),
        leftSubtitle: 'Jewellery',
        rightSubtitle: '27 Mar 2022',
        onImageTap: (i) {},
      ),
    ];
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Text(
                "On Trending",
                style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
              ),
            ],
          ),
        ),
        CustomCarouselSlider(
          items: itemList,
          height: 300,
          subHeight: 100,
          width: MediaQuery.of(context).size.width * .9,
          autoplay: true,
        ),
      ],
    );
  }
}
