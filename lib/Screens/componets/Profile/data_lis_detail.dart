import 'package:flutter/material.dart';

class ListViewExample extends StatelessWidget {
  final List<String> leftTextList = [
    'Item 1',
    'Item 2',
    'Item 3',
    'Item 4',
    'Item 5',
    'Item 6',
    'Item 7',
    'Item 8',
    'Item 9',
    'Item 10',
  ];

  final List<String> rightTextList = [
    'Description 1',
    'Description 2',
    'Description 3',
    'Description 4',
    'Description 5',
    'Description 6',
    'Description 7',
    'Description 8',
    'Description 9',
    'Description 10',
  ];

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: leftTextList.length,
      itemBuilder: (context, index) {
        return ListTile(
          title: Text(
            leftTextList[index],
            style: const TextStyle(fontWeight: FontWeight.bold),
          ),
          trailing: Text(
            rightTextList[index],
            style: const TextStyle(color: Colors.blue),
          ),
        );
      },
    );
  }
}
