import 'package:flutter/material.dart';

class ProfileLayout extends StatelessWidget {
  ProfileLayout({Key? key}) : super(key: key);
  final List<String> items = List<String>.generate(4, (i) => '$i');

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Text("My Profile"),
            Row(
              children: [
                SizedBox(
                  width: 88,
                  height: 101,
                  child: Image.asset("assets/images/profilepic.webp"),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: const [
                      Text(
                        "Mohammad Fauzi",
                        style: TextStyle(
                          color: Color.fromARGB(211, 224, 183, 0),
                        ),
                      ),
                      Text(
                        "Job :",
                        style: TextStyle(
                          color: Colors.grey,
                        ),
                      ),
                      Text(
                        "Nik :",
                        style: TextStyle(
                          color: Colors.grey,
                        ),
                      ),
                      Text(
                        "Devisi :",
                        style: TextStyle(
                          color: Colors.grey,
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
            Column(
              children: [
                ElevatedButton(
                  onPressed: () {},
                  style: ElevatedButton.styleFrom(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(
                          20.0), // Set the border radius value
                    ),
                    backgroundColor:
                        Colors.black, // Set the button background color
                  ),
                  child: const Text("Ganti Passwords"),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
