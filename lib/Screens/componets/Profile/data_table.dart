import 'package:flutter/material.dart';

class DataTableExample extends StatelessWidget {
  DataTableExample({super.key});
  final List<DataRow> rows = [
    const DataRow(cells: [
      DataCell(Text('1')),
      DataCell(Text('ABC123')),
      DataCell(Text('Violation 1')),
    ]),
    const DataRow(cells: [
      DataCell(Text('2')),
      DataCell(Text('DEF456')),
      DataCell(Text('Violation 2')),
    ]),
    const DataRow(cells: [
      DataCell(Text('3')),
      DataCell(Text('GHI789')),
      DataCell(Text('Violation 3')),
    ]),
    const DataRow(cells: [
      DataCell(Text('4')),
      DataCell(Text('JKL012')),
      DataCell(Text('Violation 4')),
    ]),
    const DataRow(cells: [
      DataCell(Text('5')),
      DataCell(Text('MNO345')),
      DataCell(Text('Violation 5')),
    ]),
  ];
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: Axis.vertical,
      child: DataTable(
        columns: const <DataColumn>[
          DataColumn(label: Text('No')),
          DataColumn(label: Text('Code\nSurat')),
          DataColumn(label: Text('Pelanggaran\nBerupa')),
        ],
        rows: rows,
      ),
    );
  }
}
