import 'package:flutter/material.dart';

import 'data_lis_detail.dart';
import 'data_table.dart';

class TabbarView extends StatefulWidget {
  const TabbarView({super.key});

  @override
  State<StatefulWidget> createState() => TabbarViewState();
}

class TabbarViewState extends State<TabbarView> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 4,
      child: Scaffold(
        appBar: PreferredSize(
          preferredSize: const Size.fromHeight(kToolbarHeight),
          child: Container(
            color: const Color.fromARGB(255, 255, 255, 255),
            child: SafeArea(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Row(
                    children: const <Widget>[
                      Expanded(
                        child: TabBar(
                          tabs: [
                            Tab(
                              text: "Data\nPribadi",
                            ),
                            Tab(
                              text: "Data\nKeluarga",
                            ),
                            Tab(
                              text: "Data\nPendidikan",
                            ),
                            Tab(
                              text: "Data \nST/SP",
                            ),
                          ],
                          labelColor: Color.fromARGB(211, 224, 183,
                              0), // Set the color for the selected tab
                          unselectedLabelColor: Colors
                              .grey, // Set the color for the unselected tabs
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
        body: TabBarView(
          children: <Widget>[
            ListViewExample(),
            ListViewExample(),
            ListViewExample(),
            Card(child: DataTableExample()),
          ],
        ),
      ),
    );
  }
}
