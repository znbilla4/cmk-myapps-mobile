import 'package:flutter/material.dart';

class DetailProfile extends StatefulWidget {
  const DetailProfile({Key? key}) : super(key: key);

  @override
  _DetailProfileState createState() => _DetailProfileState();
}

class _DetailProfileState extends State<DetailProfile> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: const [
        Card(
          child: ListTile(
            leading: Icon(Icons.book_online),
            title: Text("Function"),
            subtitle: Text("External Supporting"),
            // trailing: const Icon(Icons.add_a_photo),
          ),
        ),
        Card(
          child: ListTile(
            leading: Icon(Icons.location_city),
            title: Text("Lokasi Kerja"),
            subtitle: Text("Synthesis Square Jakarta"),
            // trailing: const Icon(Icons.add_a_photo),
          ),
        ),
        Card(
          child: ListTile(
            leading: Icon(Icons.punch_clock),
            title: Text("Jam Kerja"),
            subtitle: Text("Jam Kantor 1 (8.00 - 17.00)"),
            // trailing: const Icon(Icons.add_a_photo),
          ),
        ),
        Card(
          child: ListTile(
            leading: Icon(Icons.document_scanner),
            title: Text("Habis Kontrak:"),
            subtitle: Text("25 April 2024"),
            // trailing: const Icon(Icons.add_a_photo),
          ),
        )
      ],
    );
  }
}
