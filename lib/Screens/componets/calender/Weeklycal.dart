import 'package:flutter/material.dart';
import 'package:flutter_calendar_week/flutter_calendar_week.dart';
import 'package:intl/intl.dart';


class CalenderWeekly extends StatefulWidget {
  const CalenderWeekly({super.key});

  @override
  _CalenderWeeklyState createState() => _CalenderWeeklyState();
}

class _CalenderWeeklyState extends State<CalenderWeekly> {
  final CalendarWeekController _controller = CalendarWeekController();

  @override
  Widget build(BuildContext context) =>  SizedBox(
    height: 500,
    child: Column(
      children: [
            Container(
                decoration: BoxDecoration(boxShadow: [
                  BoxShadow(
                      color: Colors.black.withOpacity(0.2),
                      blurRadius: 10,
                      spreadRadius: 1)
                ]),
                child: CalendarWeek(
                  controller: _controller,
                  height: 100,
                  showMonth: true,
                  minDate: DateTime.now().add(
                    Duration(days: -3),
                  ),
                  maxDate: DateTime.now().add(
                    Duration(days: 3),
                  ),
                  onDatePressed: (DateTime datetime) {
                    // Do something
                    setState(() {});
                  },
                  onDateLongPressed: (DateTime datetime) {
                    // Do something
                  },
                  onWeekChanged: () {
                    // Do something
                  },
                  monthViewBuilder: (DateTime time) => Align(
                    alignment: FractionalOffset.center,
                    child: Container(
                        margin: const EdgeInsets.symmetric(vertical: 4),
                        child: Text(
                          DateFormat.yMMMM().format(time),
                          overflow: TextOverflow.ellipsis,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: Colors.blue, fontWeight: FontWeight.w600),
                        )),
                  ),
                  decorations: [
                    DecorationItem(
                        decorationAlignment: FractionalOffset.bottomRight,
                        date: DateTime.now(),
                        decoration: Icon(
                          Icons.today,
                          color: Colors.blue,
                        )),
                    DecorationItem(
                        date: DateTime.now().add(Duration(days: 3)),
                        decoration: Text(
                          'Holiday',
                          style: TextStyle(
                            color: Colors.brown,
                            fontWeight: FontWeight.w600,
                          ),
                        )),
                  ],
                )),
            Expanded(
              child: Center(
                child: Text(
                  '${_controller.selectedDate.day}/${_controller.selectedDate.month}/${_controller.selectedDate.year}',
                  style: TextStyle(fontSize: 30),
                ),
              ),
            )
          ]),
  );
}