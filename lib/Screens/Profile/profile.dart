import 'package:flutter/material.dart';

import '../Drawser/drawer.dart';
import '../assets/notif.dart';
import '../componets/Profile/detail_profile.dart';
import '../componets/Profile/profil_layout.dart';
import '../componets/Profile/tabbar_view.dart';

class ProfilePage extends StatelessWidget {
  const ProfilePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black,
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
              icon: const Icon(Icons.menu),
              onPressed: () {
                Scaffold.of(context).openDrawer();
              },
            );
          },
        ),
        centerTitle: true,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              'assets/images/CMK-logo-header.png', // Replace with your logo image path
              height: 28.0, // Adjust the logo height as needed
            ),
            const Text(
              "CMK",
              style: TextStyle(
                color: Color.fromRGBO(234, 190, 1, 1),
              ),
            )
          ],
        ),
        actions: [
          IconButton(
            icon: const Icon(Icons.notifications_active, size: 25),
            onPressed: () {
              NotificationDialog.show(context);
            },
          ),
          IconButton(
            icon: const CircleAvatar(
              backgroundImage: AssetImage(
                  'assets/images/profilepic.webp'), // Replace with your profile picture image path
            ),
            onPressed: () {},
          ),
        ],
      ),
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        physics: const AlwaysScrollableScrollPhysics(),
        child: Column(
          children: [
            ProfileLayout(),
            const SizedBox(height: 320, child: DetailProfile()),
            const SizedBox(
              height: 10,
            ),
            const SizedBox(height: 400, child: TabbarView()),
          ],
        ),
      ),
      drawer: const MyDrawer(),
    );
  }
}
