import 'package:expandable_text/expandable_text.dart';
import 'package:flutter/material.dart';

import '../componets/main_screen_widgets/event.dart';
import '../componets/main_screen_widgets/widget_new.dart';
import 'discus.dart';

class NewsPage extends StatelessWidget {
  final String datadetail;
  final String datatype;
  final String dataimage;

  NewsPage(this.datadetail, this.datatype, this.dataimage);

  @override
  Widget build(BuildContext context) {
    TextEditingController _textEditingController = TextEditingController();

    void _sendMessage() {
      final String message = _textEditingController.text;
      // Process or send the message here
      print('Sending message: $message');
      // Clear the text field
      _textEditingController.clear();
    }

    return Scaffold(
      appBar: AppBar(
        title: const Text('News detail Page'),
        backgroundColor: Colors.black,
      ),
      body: SingleChildScrollView(
        physics: const AlwaysScrollableScrollPhysics(),
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Column(
            children: [
              const Text(
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
                style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
              ),
              const SizedBox(
                height: 10,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: const [Text("postedby: Fauzi"), Text("2023/05/21")],
              ),
              const SizedBox(
                height: 10,
              ),
              SizedBox(
                height: 300,
                width: 400,
                child: ListView(
                  children: [
                    ListTile(
                      title: Image.asset(dataimage),
                      subtitle: ExpandableText(
                        'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.',
                        expandText: 'show more',
                        collapseText: 'show less',
                        maxLines: 2,
                        linkColor: Colors.blue,
                      ),
                    )
                  ],
                ),
              ),
              SizedBox(
                  height: 130,
                  child: Card(
                    child: Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: const [
                              Icon(Icons.event),
                              Text("05 Mei 2023")
                            ],
                          ),
                          Row(
                            children: const [Icon(Icons.timer), Text("10:00")],
                          ),
                          Row(
                            children: const [
                              Icon(Icons.location_city),
                              Text(
                                  "Bookmarksgrove right at\nthe coast of the Semantics")
                            ],
                          )
                        ],
                      ),
                    ),
                  )),
              SizedBox(
                height: 270,
                width: 370,
                child: Card(
                  elevation: 2,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      image: const DecorationImage(
                        image: AssetImage('assets/images/invbg.png'),
                        fit: BoxFit.cover,
                      ),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const SizedBox(
                            height: 20,
                          ),
                          const Text(
                            'Exclusive, you’re invited!',
                            style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                              color: Color.fromARGB(255, 255, 215, 0),
                            ),
                          ),
                          const SizedBox(height: 8),
                          const Text(
                            'Grumpy Buffalo invited you to this event.Please confirm your attendance.',
                            style: TextStyle(
                              fontSize: 16,
                              color: Colors.white,
                            ),
                          ),
                          const SizedBox(height: 16),
                          const Text(
                            'Honest Rhinos and 50 others will attend this event. ',
                            style: TextStyle(
                              fontSize: 14,
                              color: Colors.white,
                            ),
                          ),
                          Center(
                            child: ElevatedButton(
                                onPressed: () {},
                                style: ElevatedButton.styleFrom(
                                    backgroundColor:
                                        const Color.fromARGB(255, 255, 215, 0)),
                                child: const Text(
                                  "I will Attend this Event",
                                  style: TextStyle(
                                      color: Color.fromARGB(255, 0, 0, 0)),
                                )),
                          ),
                          const Center(
                              child: Text(
                            "Confirm before 20/04/2023",
                            style: TextStyle(
                              fontSize: 14,
                              color: Colors.white,
                            ),
                          ))
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              Card(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text("Ask a Question"),
                      const SizedBox(
                        height: 10,
                      ),
                      Container(
                        width: 350,
                        height: 50,
                        decoration: BoxDecoration(
                          color: const Color.fromARGB(255, 243, 243, 243),
                          borderRadius: BorderRadius.circular(33),
                        ),
                        child: Row(
                          children: [
                            const Expanded(
                              child: Padding(
                                padding: EdgeInsets.only(left: 20.0),
                                child: TextField(
                                  cursorColor: Colors.black,
                                  style: TextStyle(color: Colors.white),
                                  decoration: InputDecoration(
                                      hintText: "Write your question here...",
                                      hintStyle: TextStyle(
                                          color:
                                              Color.fromARGB(255, 255, 215, 0)),
                                      border: InputBorder.none),
                                ),
                              ),
                            ),
                            IconButton(
                              onPressed: _sendMessage,
                              icon: const Icon(Icons.send),
                              color: const Color.fromARGB(255, 255, 215, 0),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
              DiscussionsForm(),
              NewsWidget(),
              const SizedBox(
                height: 20,
              ),
              EventWidget()
            ],
          ),
        ),
      ),
    );
  }
}
