import 'package:flutter/material.dart';

import '../../Data/comment.dart';

class DiscussionsForm extends StatefulWidget {
  @override
  _DiscussionsFormState createState() => _DiscussionsFormState();
}

class _DiscussionsFormState extends State<DiscussionsForm> {
  void addReply(Comment comment) {
    showDialog(
      context: context,
      builder: (context) {
        TextEditingController replyController = TextEditingController();
        return AlertDialog(
          title: const Text('Add Reply'),
          content: TextField(
            controller: replyController,
            decoration: const InputDecoration(
              hintText: 'Enter your reply',
            ),
          ),
          actions: [
            TextButton(
              onPressed: () => Navigator.pop(context),
              child: const Text('Cancel'),
            ),
            TextButton(
              onPressed: () {
                final newReply = Reply(
                  id: '4',
                  author: 'John Smith',
                  content: replyController.text,
                  createdAt: DateTime.now(),
                );
                setState(() {
                  comment.replies.add(newReply);
                });
                Navigator.pop(context);
              },
              child: const Text('Add'),
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    final commentCount = dummyComments.length;
    final replyCount = dummyComments.fold<int>(
        0, (previousValue, comment) => previousValue + comment.replies.length);
    final itemCount = commentCount + replyCount;
    final itemHeight = 55.0; // Desired height for each item
    final listViewHeight = itemHeight * itemCount;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const SizedBox(
          height: 17,
        ),
        const Text("Discussions :"),
        SizedBox(
          height: listViewHeight,
          width: 400,
          child: ListView.builder(
            physics: const AlwaysScrollableScrollPhysics(),
            itemCount: itemCount,
            itemBuilder: (context, index) {
              if (index < commentCount) {
                // Render comment
                final comment = dummyComments[index];
                return Card(
                  color: Colors
                      .grey[200], // Adjust the background color of comment card
                  child: ListTile(
                    title: Text(comment.content),
                    subtitle: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('By ${comment.author}'),
                        const SizedBox(height: 4),
                        ...comment.replies.map((reply) {
                          return Card(
                            color: Colors.grey,
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text(
                                '- ${reply.author}: ${reply.content}',
                                style: const TextStyle(color: Colors.white),
                              ),
                            ),
                          );
                        }).toList(),
                      ],
                    ),
                    trailing: IconButton(
                      icon: const Icon(Icons.reply),
                      onPressed: () => addReply(comment),
                    ),
                  ),
                );
              } else {
                // Render reply
                final replyIndex = index - commentCount;
                int commentIndex = 0;
                int accumulatedReplyCount =
                    dummyComments[commentIndex].replies.length;
                while (replyIndex >= accumulatedReplyCount &&
                    commentIndex < commentCount - 1) {
                  commentIndex++;
                  accumulatedReplyCount +=
                      dummyComments[commentIndex].replies.length;
                }
              }
              return const SizedBox(); // Return an empty SizedBox if the index is invalid
            },
          ),
        ),
      ],
    );
  }
}
