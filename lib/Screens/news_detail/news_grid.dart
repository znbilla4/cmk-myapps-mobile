import 'package:flutter/material.dart';

import '../../Data/grid_news.dart';
import 'category.dart';
import 'news_detail.dart';

class YourPage extends StatefulWidget {
  @override
  _YourPageState createState() => _YourPageState();
}

class _YourPageState extends State<YourPage> {
  List<String> items = List.generate(100, (index) => 'Item $index');
  TextEditingController searchController = TextEditingController();
  List<Item> filteredItems = [];

  int itemsPerPage = 8;
  int currentPage = 1;
  int selectedItemIndex = 1;

  @override
  void initState() {
    super.initState();
    filteredItems.addAll(itemList);
  }

  void filterItems(String query) {
    if (query.isNotEmpty) {
      setState(() {
        filteredItems = itemList
            .where(
                (item) => item.type.toLowerCase().contains(query.toLowerCase()))
            .toList();
      });
    } else {
      setState(() {
        filteredItems = List.from(itemList);
      });
    }
  }

  void handleItemTap(int index) {
    if (index == selectedItemIndex) {
      String datadetail = filteredItems[index].detail;
      String dataimage = filteredItems[index].image;
      String datatype = filteredItems[index].type;
      Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => NewsPage(datadetail, datatype, dataimage)),
      );
    } else {
      setState(() {
        selectedItemIndex = index;
      });
    }
  }

  void loadMoreItems() {
    setState(() {
      itemsPerPage += 2;
    });
  }

  @override
  Widget build(BuildContext context) {
    int startIndex = (currentPage - 1) * itemsPerPage;
    int endIndex = startIndex + itemsPerPage;
    endIndex =
        endIndex > filteredItems.length ? filteredItems.length : endIndex;
    List<Item> paginatedItems = filteredItems.sublist(startIndex, endIndex);

    return SizedBox(
      height: 500,
      width: 500,
      child: Column(
        children: [
          Container(
            width: 350,
            height: 50,
            decoration: BoxDecoration(
              color: const Color.fromARGB(255, 243, 243, 243),
              borderRadius: BorderRadius.circular(33),
            ),
            child: const Expanded(
              child: Padding(
                padding: EdgeInsets.only(left: 20.0),
                child: TextField(
                  cursorColor: Colors.black,
                  style: TextStyle(color: Colors.white),
                  decoration: InputDecoration(
                      hintText: "Find Events...",
                      hintStyle:
                          TextStyle(color: Color.fromARGB(255, 255, 215, 0)),
                      border: InputBorder.none),
                ),
              ),
            ),
          ),
          HorizontalCategoryList(),
          Expanded(
            child: GridView.builder(
              scrollDirection: Axis.horizontal,
              gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                childAspectRatio: 1,
                crossAxisCount: 2, // Number of columns
                mainAxisSpacing: 1.0, // Spacing between rows
                crossAxisSpacing: 8.0, // Spacing between columns
              ),
              itemCount: paginatedItems.length,
              itemBuilder: (context, index) {
                Item item = paginatedItems[index];
                return InkWell(
                  onTap: () => handleItemTap(index),
                  child: Card(
                    child: Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Column(
                        children: [
                          Image.asset(
                            item.image,
                            width: 150,
                            height: 100,
                            fit: BoxFit.cover,
                          ),
                          Text(item.type),
                          Text(item.detail),
                        ],
                      ),
                    ),
                  ),
                );
              },
            ),
          ),
          TextButton(
            onPressed: loadMoreItems,
            child: const Text(
              'Load More',
              style: TextStyle(color: Color.fromARGB(255, 255, 215, 0)),
            ),
          ),
        ],
      ),
    );
  }
}
