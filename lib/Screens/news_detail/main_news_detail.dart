import 'package:flutter/material.dart';
import 'package:myappsnew/Screens/componets/main_screen_widgets/event.dart';
import 'package:myappsnew/Screens/componets/main_screen_widgets/widget_new.dart';
import 'package:myappsnew/Screens/Profile/profile.dart';

import '../assets/notif.dart';

import 'news_grid.dart';

class NewsDetial extends StatelessWidget {
  const NewsDetial({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black,
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
              icon: const Icon(Icons.menu),
              onPressed: () {
                Scaffold.of(context).openDrawer();
              },
            );
          },
        ),
        centerTitle: true,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              'assets/images/CMK-logo-header.png', // Replace with your logo image path
              height: 28.0, // Adjust the logo height as needed
            ),
            const Text("CMK")
          ],
        ),
        actions: [
          IconButton(
            icon: const Icon(Icons.notifications_active, size: 25),
            onPressed: () {
              NotificationDialog.show(context);
            },
          ),
          IconButton(
            icon: const CircleAvatar(
              backgroundImage: AssetImage(
                  'assets/images/profilepic.webp'), // Replace with your profile picture image path
            ),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => const ProfilePage()),
              );
            },
          ),
        ],
      ),
      body: SingleChildScrollView(
        physics: AlwaysScrollableScrollPhysics(),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: [
              const SizedBox(
                height: 20,
              ),
              YourPage(),
              Divider(
                thickness: 0.5,
              ),
              NewsWidget(),
              const SizedBox(
                height: 20,
              ),
              EventWidget()
            ],
          ),
        ),
      ),
    );
  }
}
