import 'package:flutter/material.dart';

class BirthdayAlert {
  static void show(BuildContext context, String userName, String userBirthday) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Happy Birthday, $userName!'),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                  'Wishing you a fantastic birthday filled with joy and happiness.'),
              SizedBox(height: 10),
              Text('May your day be as special as you are, $userName!'),
              SizedBox(height: 10),
              Text('Date of Birth: $userBirthday'),
            ],
          ),
          actions: [
            TextButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: Text('OK'),
            ),
          ],
        );
      },
    );
  }
}
