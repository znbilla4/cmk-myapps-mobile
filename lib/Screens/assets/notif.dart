import 'package:flutter/material.dart';

class NotificationDialog {
  static void show(BuildContext context) {
    final List<Map<String, String>> dummyList = [
      {
        'title': 'Item 1',
        'subtitle': 'Subtitle 1',
        'leading': 'A',
      },
      {
        'title': 'Item 2',
        'subtitle': 'Subtitle 2',
        'leading': 'B',
      },
      {
        'title': 'Item 3',
        'subtitle': 'Subtitle 3',
        'leading': 'C',
      },
      {
        'title': 'Item 4',
        'subtitle': 'Subtitle 4',
        'leading': 'D',
      },
      {
        'title': 'Item 5',
        'subtitle': 'Subtitle 5',
        'leading': 'E',
      },
    ];
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('Notification'),
          content: SizedBox(
            width: 300, // Adjust the width as needed
            height: 400, // Adjust the height as needed
            child: SingleChildScrollView(
              child: Column(
                children: [
                  //Text('Notification content goes here.'),
                  const Divider(
                    thickness: 0.5,
                  ),
                  ListView.builder(
                    shrinkWrap: true,
                    itemCount: dummyList.length,
                    itemBuilder: (context, index) {
                      final item = dummyList[index];
                      return Card(
                        child: ListTile(
                          leading: CircleAvatar(
                            child: Text(item['leading']!),
                          ),
                          title: Text(item['title']!),
                          subtitle: Text(item['subtitle']!),
                          onTap: () {
                            // Handle item tap
                            //  print('Item ${index + 1} tapped');
                          },
                        ),
                      );
                    },
                  ),
                ],
              ),
            ),
          ),
          actions: [
            TextButton(
              child: const Text('Close'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}
