import 'package:flutter/material.dart';

import '../news_detail/main_news_detail.dart';

class MyDrawer extends StatefulWidget {
  const MyDrawer({super.key});

  @override
  _MyDrawerState createState() => _MyDrawerState();
}

class _MyDrawerState extends State<MyDrawer> {
  int currentPage = 1;
  int itemsPerPage = 10;
  int selectedItemIndex = 0;
  void handleItemTap(int index) {
    if (index == 5) {
      Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) =>
                NewsDetial()), // Replace `NewsPage` with the appropriate widget representing your news page
      );
    } else {
      setState(() {
        selectedItemIndex = index;
        currentPage = (index ~/ itemsPerPage) + 1;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Container(
        color: const Color.fromARGB(255, 0, 0, 0),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: ListView(
            padding: EdgeInsets.zero,
            children: [
              const SizedBox(
                height: 110,
              ),
              Card(
                color: const Color.fromARGB(255, 66, 66, 66),
                child: ListTile(
                  leading: const Icon(Icons.square),
                  title: Text(
                    'Dashboard',
                    style: TextStyle(
                      color:
                          selectedItemIndex == 0 ? Colors.yellow : Colors.white,
                      fontSize: 16.0,
                    ),
                  ),
                  selected: selectedItemIndex == 0,
                  selectedColor: Colors.yellow,
                  onTap: () => handleItemTap(0),
                ),
              ),
              Card(
                color: const Color.fromARGB(255, 66, 66, 66),
                child: ListTile(
                  leading: const Icon(Icons.person_rounded),
                  title: Text(
                    'Emlpoyee Self Service',
                    style: TextStyle(
                      color:
                          selectedItemIndex == 1 ? Colors.yellow : Colors.white,
                      fontSize: 16.0,
                    ),
                  ),
                  selected: selectedItemIndex == 1,
                  selectedColor: Colors.yellow,
                  onTap: () => handleItemTap(1),
                ),
              ),
              Card(
                color: const Color.fromARGB(255, 66, 66, 66),
                child: ListTile(
                  leading: const Icon(Icons.menu),
                  title: Text(
                    'Visit Store Checklist',
                    style: TextStyle(
                      color:
                          selectedItemIndex == 2 ? Colors.yellow : Colors.white,
                      fontSize: 16.0,
                    ),
                  ),
                  selected: selectedItemIndex == 2,
                  selectedColor: Colors.yellow,
                  onTap: () => handleItemTap(2),
                ),
              ),
              Card(
                color: const Color.fromARGB(255, 66, 66, 66),
                child: ListTile(
                  leading: const Icon(Icons.money),
                  title: Text(
                    'Reimburse Jamuan',
                    style: TextStyle(
                      color:
                          selectedItemIndex == 3 ? Colors.yellow : Colors.white,
                      fontSize: 16.0,
                    ),
                  ),
                  selected: selectedItemIndex == 3,
                  selectedColor: Colors.yellow,
                  onTap: () => handleItemTap(3),
                ),
              ),
              Card(
                color: const Color.fromARGB(255, 66, 66, 66),
                child: ListTile(
                  leading: const Icon(Icons.edit_document),
                  title: Text(
                    'Documents',
                    style: TextStyle(
                      color:
                          selectedItemIndex == 4 ? Colors.yellow : Colors.white,
                      fontSize: 16.0,
                    ),
                  ),
                  selected: selectedItemIndex == 4,
                  selectedColor: Colors.yellow,
                  onTap: () => handleItemTap(4),
                ),
              ),
              Card(
                color: const Color.fromARGB(255, 66, 66, 66),
                child: ListTile(
                  leading: const Icon(Icons.newspaper),
                  title: Text(
                    'Whats New',
                    style: TextStyle(
                      color:
                          selectedItemIndex == 5 ? Colors.yellow : Colors.white,
                      fontSize: 16.0,
                    ),
                  ),
                  selected: selectedItemIndex == 5,
                  selectedColor: Colors.yellow,
                  onTap: () => handleItemTap(5),
                ),
              ),
              Card(
                color: const Color.fromARGB(255, 66, 66, 66),
                child: ListTile(
                  leading: const Icon(Icons.person_rounded),
                  title: Text(
                    'PMS - Holding',
                    style: TextStyle(
                      color:
                          selectedItemIndex == 6 ? Colors.yellow : Colors.white,
                      fontSize: 16.0,
                    ),
                  ),
                  selected: selectedItemIndex == 6,
                  selectedColor: Colors.yellow,
                  onTap: () => handleItemTap(6),
                ),
              ),
              Card(
                color: const Color.fromARGB(255, 66, 66, 66),
                child: ListTile(
                  leading: const Icon(Icons.person_rounded),
                  title: Text(
                    'PMS - Retail',
                    style: TextStyle(
                      color:
                          selectedItemIndex == 7 ? Colors.yellow : Colors.white,
                      fontSize: 16.0,
                    ),
                  ),
                  selected: selectedItemIndex == 7,
                  selectedColor: Colors.yellow,
                  onTap: () => handleItemTap(7),
                ),
              ),
              Card(
                color: const Color.fromARGB(255, 66, 66, 66),
                child: ListTile(
                  leading: const Icon(Icons.settings),
                  title: Text(
                    'Settings',
                    style: TextStyle(
                      color:
                          selectedItemIndex == 8 ? Colors.yellow : Colors.white,
                      fontSize: 16.0,
                    ),
                  ),
                  selected: selectedItemIndex == 8,
                  selectedColor: Colors.yellow,
                  onTap: () => handleItemTap(8),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
