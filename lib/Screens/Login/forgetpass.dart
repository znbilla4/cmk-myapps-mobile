import 'package:flutter/material.dart';

class ForgetPass extends StatefulWidget {
  const ForgetPass({Key? key}) : super(key: key);

  @override
  _ForgetPassState createState() => _ForgetPassState();
}

class _ForgetPassState extends State<ForgetPass> {
  TextEditingController _userIdController = TextEditingController();
  TextEditingController userInput = TextEditingController();
  bool _userNotFound = false;
  String? errorText;
  String text = "";

  void _checkUser() {
    // You can implement your own logic here to check if the user exists.
    // For this example, we're assuming the user is found if their ID is not empty.
    if (_userIdController.text.isNotEmpty) {
      setState(() {
        _userNotFound = false;
      });
    } else {
      setState(() {
        _userNotFound = true;
      });
    }
  }

  void _goBackToLogin() {
    setState(() {
      _userNotFound = false;
      _userIdController.clear();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/images/bglogin.png'),
            fit: BoxFit.fill,
          ),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Card(
                color: const Color.fromARGB(17, 0, 0, 0),
                child: Column(
                  children: [
                    SizedBox(
                      height: 30,
                    ),
                    Text(
                      "Forget Password",
                      style: TextStyle(
                          fontSize: 30,
                          color: Colors.white,
                          fontWeight: FontWeight.bold),
                    ),
                    Text(
                      "Don’t worry, we'll help you regain access to your account",
                      style: TextStyle(fontSize: 15, color: Colors.white),
                    ),
                    SizedBox(
                      height: 25,
                    ),
                    Card(
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                          children: [
                            TextFormField(
                              controller: userInput,
                              style: const TextStyle(
                                fontSize: 24,
                                color: Colors.blue,
                                fontWeight: FontWeight.w600,
                              ),
                              onChanged: (value) {
                                setState(() {
                                  if (value.isEmpty) {
                                    errorText = "Form cannot be empty";
                                  } else {
                                    errorText = null;
                                  }
                                  userInput.text = value.toString();
                                });
                              },
                              decoration: InputDecoration(
                                focusColor: Colors.white,
                                prefixIcon: const Icon(
                                  Icons.person_outline_rounded,
                                  color: Colors.grey,
                                ),
                                errorText: errorText,
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10.0),
                                ),
                                focusedBorder: OutlineInputBorder(
                                  borderSide: const BorderSide(
                                    color: Colors.blue,
                                    width: 1.0,
                                  ),
                                  borderRadius: BorderRadius.circular(10.0),
                                ),
                                filled: true,
                                fillColor: Colors.white,
                                hintText: "User ID",
                                hintStyle: const TextStyle(
                                  color: Colors.grey,
                                  fontSize: 16,
                                  fontFamily: "verdana_regular",
                                  fontWeight: FontWeight.w400,
                                ),
                                labelText: 'User ID',
                                labelStyle: const TextStyle(
                                  color: Colors.grey,
                                  fontSize: 16,
                                  fontFamily: "verdana_regular",
                                  fontWeight: FontWeight.w400,
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 15,
                            ),
                            if (_userNotFound)
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(
                                  'User not found!',
                                  style: TextStyle(color: Colors.red),
                                ),
                              ),
                            MaterialButton(
                              minWidth: 350,
                              color: Colors.amberAccent,
                              shape: RoundedRectangleBorder(
                                  borderRadius: new BorderRadius.circular(12)),
                              onPressed: _checkUser,
                              child: Text('Continue'),
                            ),
                            TextButton(
                              onPressed: _goBackToLogin,
                              child: Text('Go Back to Login'),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
