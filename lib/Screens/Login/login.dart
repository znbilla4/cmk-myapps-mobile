import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:myappsnew/Screens/Main_Screen/main_screen.dart';

import 'forgetpass.dart';

class LoginScreen extends StatefulWidget {
  LoginScreen({super.key});

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  bool _passwordInVisible = true;
  TextEditingController userInput = TextEditingController();
  TextEditingController passwordInput = TextEditingController();
  final Shader linearGradient = LinearGradient(
    colors: <Color>[
      Color.fromARGB(255, 252, 255, 61),
      Color.fromARGB(225, 217, 199, 0)
    ],
  ).createShader(Rect.fromLTWH(0.0, 0.0, 200.0, 70.0));
  String? errorText;
  String text = "";

  void _handleLogin() {
    // Simulate a successful login
    setState(() {
      // You can set any condition that represents a successful login
      bool successfulLogin = true;

      if (successfulLogin) {
        // Pass the user data to the MainScreen
        Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
            builder: (context) => MainScreen(
              onLoginSuccess: _handleLogin,
            ),
          ),
          (Route<dynamic> route) => false,
        );
      } else {
        // Handle unsuccessful login here
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: const BoxDecoration(
          image: DecorationImage(
              image: AssetImage('assets/images/bglogin.png'), fit: BoxFit.fill),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
                height: 30,
                width: 60,
                child: Image.asset('assets/images/CMK-logo-header.png')),
            const SizedBox(
              height: 20,
            ),
            const Text(
              "PT. CENTRAL MEGA KENCANA",
              style: TextStyle(color: Color.fromARGB(255, 255, 255, 255)),
            ),
            Padding(
              padding: const EdgeInsets.all(15.0),
              child: Card(
                color: Color.fromARGB(99, 29, 29, 29),
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 20, bottom: 0),
                      child: Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Container(
                                  height: 45,
                                  width: 110,
                                  child: Image.asset('assets/images/F.png')),
                              Container(
                                  height: 35,
                                  width: 110,
                                  child: Image.asset('assets/images/TP.png')),
                              Container(
                                  height: 35,
                                  width: 110,
                                  child: Image.asset('assets/images/MD.png')),
                            ],
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Visibility(
                            visible: errorText != null,
                            child: Container(
                              margin: const EdgeInsets.only(top: 8.0),
                              child: Card(
                                color: const Color.fromARGB(124, 255, 0, 0),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    const Icon(
                                      Icons.error,
                                      color: Color.fromARGB(255, 255, 255, 255),
                                    ),
                                    const SizedBox(width: 6),
                                    Text(
                                      errorText ?? '',
                                      style: const TextStyle(
                                        color:
                                            Color.fromARGB(255, 255, 255, 255),
                                        fontSize: 10,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          TextFormField(
                            controller: userInput,
                            style: const TextStyle(
                              fontSize: 24,
                              color: Color.fromARGB(255, 255, 255, 255),
                              fontWeight: FontWeight.w600,
                            ),
                            onChanged: (value) {
                              setState(() {
                                if (value.isEmpty) {
                                  errorText = "Form cannot be empty";
                                } else {
                                  errorText = null;
                                }
                                userInput.text = value.toString();
                              });
                            },
                            decoration: InputDecoration(
                              focusColor: Colors.white,
                              // prefixIcon: const Icon(
                              //   Icons.person_outline_rounded,
                              //   color: Color.fromARGB(255, 255, 255, 255),
                              // ),
                              //  errorText:
                              //    errorText, // Use the errorText variable
                              border: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Color.fromARGB(255, 232, 178, 0)),
                                borderRadius: BorderRadius.circular(10.0),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderSide: const BorderSide(
                                  color: Color.fromARGB(255, 232, 178, 0),
                                  width: 1.0,
                                ),
                                borderRadius: BorderRadius.circular(10.0),
                              ),
                              filled: true,
                              fillColor: const Color.fromARGB(255, 0, 0, 0),
                              // hintText: "User ID",
                              // hintStyle: const TextStyle(
                              //   color: Color.fromARGB(255, 232, 178, 0),
                              //   fontSize: 16,
                              //   fontFamily: "verdana_regular",
                              //   fontWeight: FontWeight.w400,
                              // ),
                              labelText: 'User ID',
                              labelStyle: const TextStyle(
                                color: Color.fromARGB(255, 150, 150, 150),
                                fontSize: 16,
                                fontFamily: "verdana_regular",
                                fontWeight: FontWeight.w400,
                              ),
                            ),
                          ),
                          // Text(userInput.toString()),

                          const SizedBox(height: 20),
                          TextFormField(
                            controller: passwordInput,
                            style: const TextStyle(
                              fontSize: 24,
                              color: Color.fromARGB(255, 255, 255, 255),
                              fontWeight: FontWeight.w600,
                            ),
                            onChanged: (value) {
                              setState(() {
                                if (value.isEmpty) {
                                  errorText = "Form cannot be empty";
                                } else {
                                  errorText = null;
                                }
                                passwordInput.text = value.toString();
                              });
                            },
                            decoration: InputDecoration(
                                focusColor: Colors.white,
                                // prefixIcon: const Icon(
                                //   Icons.password,
                                //   color: Color.fromARGB(255, 232, 178, 0),
                                // ),
                                //  errorText:
                                //    errorText, // Use the errorText variable
                                border: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: const Color.fromARGB(
                                          255, 232, 178, 0)),
                                  borderRadius: BorderRadius.circular(10.0),
                                ),
                                focusedBorder: OutlineInputBorder(
                                  borderSide: const BorderSide(
                                    color: Color.fromARGB(255, 232, 178, 0),
                                    width: 1.0,
                                  ),
                                  borderRadius: BorderRadius.circular(10.0),
                                ),
                                filled: true,
                                fillColor: const Color.fromARGB(255, 0, 0, 0),
                                // hintText: "Password",
                                // hintStyle: const TextStyle(
                                //   color: Color.fromARGB(255, 15, 0, 232),
                                //   fontSize: 16,
                                //   fontFamily: "verdana_regular",
                                //   fontWeight: FontWeight.w400,
                                // ),
                                labelText: 'Password',
                                labelStyle: const TextStyle(
                                  color: Color.fromARGB(255, 150, 150, 150),
                                  fontSize: 16,
                                  fontFamily: "verdana_regular",
                                  fontWeight: FontWeight.w400,
                                ),
                                suffixIcon: IconButton(
                                  onPressed: () {
                                    setState(() {
                                      _passwordInVisible =
                                          !_passwordInVisible; //change boolean value
                                    });
                                  },
                                  icon: Icon(
                                    _passwordInVisible
                                        ? Icons.visibility
                                        : Icons
                                            .visibility_off, //change icon based on boolean value
                                    color: Theme.of(context).primaryColorDark,
                                  ),
                                )),
                          ),
                          const SizedBox(height: 30),
                          SizedBox(
                            width: 400,
                            child: MaterialButton(
                              color: const Color.fromARGB(255, 197, 154, 0),
                              shape: RoundedRectangleBorder(
                                  borderRadius: new BorderRadius.circular(12)),
                              onPressed: () {
                                String userId = userInput.text;
                                String password = passwordInput.text;

                                if (userId.isEmpty && password.isEmpty) {
                                  setState(() {
                                    // Navigator.push(
                                    //   context,
                                    //   MaterialPageRoute(
                                    //     builder: (context) => MainScreen(
                                    //       onLoginSuccess:
                                    //           _handleLogin, // Pass the function
                                    //     ),
                                    //   ),
                                    // );
                                    // errorText =
                                    //     "Invalid user ID or password. Try again.";
                                  });
                                } else if (userId.isEmpty) {
                                  setState(() {
                                    errorText = "Invalid user ID";
                                  });
                                } else if (password.isEmpty) {
                                  setState(() {
                                    errorText = "Invalid password";
                                  });
                                } else {
                                  // Perform login or further validation logic
                                  // Clear the error message if necessary
                                  setState(() {
                                    Navigator.pushAndRemoveUntil(
                                      context,
                                      MaterialPageRoute(
                                        builder: (context) => MainScreen(
                                          onLoginSuccess: _handleLogin,
                                        ),
                                      ),
                                      (Route<dynamic> route) => false,
                                    );
                                  });
                                  // Additional logic for login or form submission
                                }
                              },
                              child: const Text("Login",
                                  style: TextStyle(fontSize: 20)),
                            ),
                          ),
                          Divider(
                            height: 10,
                          ),
                          Center(
                            child: SizedBox(
                              height: 50,
                              width: 300,
                              child: TextButton(
                                style: TextButton.styleFrom(
                                  foregroundColor:
                                      const Color.fromARGB(255, 197, 154, 0),
                                  textStyle: const TextStyle(
                                    color: Color.fromARGB(255, 0, 0, 0),
                                    fontSize: 25,
                                  ),
                                ),
                                onPressed: () {
                                  Navigator.of(context).push(
                                    MaterialPageRoute(
                                      builder: (context) => ForgetPass(),
                                    ),
                                  );
                                },
                                child: const Text('Forget Password?'),
                              ),
                            ),
                          ),
                          Divider(
                            height: 10,
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
