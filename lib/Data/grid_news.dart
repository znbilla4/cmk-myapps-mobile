class Item {
  final String image;
  final String type;
  final String detail;

  Item({
    required this.image,
    required this.type,
    required this.detail,
  });
}

List<Item> itemList = [
  Item(
    image: 'assets/images/Stock.jpeg',
    type: 'Type A',
    detail: 'Detail of Item 1',
  ),
  Item(
    image: 'assets/images/WR.jpg',
    type: 'Type B',
    detail: 'Detail of Item 2',
  ),
  Item(
    image: 'assets/images/Stock1.jpeg',
    type: 'Type C',
    detail: 'Detail of Item 3',
  ),
  Item(
    image: 'assets/images/Stock1.jpeg',
    type: 'Type C',
    detail: 'Detail of Item 4',
  ),
  Item(
    image: 'assets/images/Stock1.jpeg',
    type: 'Type C',
    detail: 'Detail of Item 5',
  ),
  Item(
    image: 'assets/images/WR.jpg',
    type: 'Type C',
    detail: 'Detail of Item 6',
  ),
  Item(
    image: 'assets/images/WR.jpg',
    type: 'Type C',
    detail: 'Detail of Item 7',
  ),
  Item(
    image: 'assets/images/Stock1.jpeg',
    type: 'Type C',
    detail: 'Detail of Item 8',
  ),
  Item(
    image: 'assets/images/Stock1.jpeg',
    type: 'Type C',
    detail: 'Detail of Item 9',
  ),
  // Add more items as needed
];
