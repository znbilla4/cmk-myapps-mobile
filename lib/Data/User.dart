class UserData {
  final String name;
  final String birthday;

  UserData({required this.name, required this.birthday});
}
