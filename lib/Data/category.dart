class Category {
  final String id;
  final String name;

  Category({
    required this.id,
    required this.name,
  });
}

class CategoryData {
  static List<Category> categories = [
    Category(
      id: '1',
      name: 'Necklaces',
    ),
    Category(
      id: '2',
      name: 'Earrings',
    ),
    Category(
      id: '3',
      name: 'Bracelets',
    ),
    Category(
      id: '4',
      name: 'Rings',
    ),
    Category(
      id: '5',
      name: 'Pendants',
    ),
    Category(
      id: '6',
      name: 'Anklets',
    ),
    Category(
      id: '7',
      name: 'Watches',
    ),
  ];
}
