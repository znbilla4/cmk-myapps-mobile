class Comment {
  final String id;
  final String author;
  final String content;
  final DateTime createdAt;
  final List<Reply> replies;

  Comment({
    required this.id,
    required this.author,
    required this.content,
    required this.createdAt,
    required this.replies,
  });
}

class Reply {
  final String id;
  final String author;
  final String content;
  final DateTime createdAt;

  Reply({
    required this.id,
    required this.author,
    required this.content,
    required this.createdAt,
  });
}

List<Comment> dummyComments = [
  Comment(
    id: '1',
    author: 'John Doe',
    content: 'This is the first comment',
    createdAt: DateTime.now().subtract(Duration(days: 2)),
    replies: [
      Reply(
        id: '1',
        author: 'Jane Smith',
        content: 'Thanks for the comment!',
        createdAt: DateTime.now().subtract(Duration(days: 1)),
      ),
    ],
  ),
  Comment(
    id: '2',
    author: 'Alice Johnson',
    content: 'I have something to say too',
    createdAt: DateTime.now().subtract(Duration(hours: 4)),
    replies: [
      Reply(
        id: '2',
        author: 'Bob Anderson',
        content: 'What is it?',
        createdAt: DateTime.now().subtract(Duration(hours: 3)),
      ),
      Reply(
        id: '3',
        author: 'Alice Johnson',
        content: 'I changed my mind. Nevermind!',
        createdAt: DateTime.now().subtract(Duration(hours: 2)),
      ),
    ],
  ),
  Comment(
    id: '3',
    author: 'John Doe',
    content: 'This is the first comment',
    createdAt: DateTime.now().subtract(Duration(days: 2)),
    replies: [],
  ),
  Comment(
    id: '4',
    author: 'John Doe',
    content: 'This is the first comment',
    createdAt: DateTime.now().subtract(Duration(days: 2)),
    replies: [],
  ),
];
