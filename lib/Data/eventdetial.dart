class EventDetails {
  final String date;
  final String time;
  final String where;

  EventDetails({
    required this.date,
    required this.time,
    required this.where,
  });
}

List<EventDetails> eventDetails = [
  EventDetails(
    date: '05 Mei 2023',
    time: '10:00',
    where: 'Bookmarksgrove right at the coast of the Semantics',
  ),
  // Add more event details entries as needed
];
